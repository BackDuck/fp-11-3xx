{-==== Низамов Нуршат, 11-304 ====-}


module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}

rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket other = if snd (getMax other) == getMinWeight other then [getMax other] else other  

getMax :: [(Integer, Integer)] -> (Integer, Integer)
getMax (first:[]) = first
getMax (first:other) = if ((fst first) `quot` (snd first)) > (fst (getMax other)) `quot` (snd (getMax other)) then first
    else getMax other
 
 
getMinWeight :: [(Integer, Integer)] -> Integer
getMinWeight (first:[]) = snd first
getMinWeight (first:other) | snd first < getMinWeight other = snd first
                  | otherwise = getMinWeight other
 
 

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo loads):other) = (orbiters other) ++ (getOrbitMachines loads)
orbiters ((Rocket _ ship):other) = (orbiters other) ++ (orbiters ship)


getOrbitMachines :: [Load a] -> [Load a]
getOrbitMachines [] = []
getOrbitMachines ((Orbiter or):other) = (Orbiter or):(getOrbitMachines other)
getOrbitMachines ((Probe p):other) = getOrbitMachines other
 
{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier ((Warp _):other) = "Kirk":(finalFrontier other)
finalFrontier ((BeamUp _):other) = "Kirk":(finalFrontier other)
finalFrontier ((IsDead _):other) = "McCoy":(finalFrontier other)
finalFrontier ((LiveLongAndProsper):other) = "Spock":(finalFrontier other)
finalFrontier ((Fascinating):other) = "Spock":(finalFrontier other)

