data Term = Var String | Lambda String Term | Apply Term Term
    deriving (Eq)

instance Show Term where
    show t = case t of
       Var x -> x
       Lambda x t1 -> "(\\" ++ x ++ "." ++ show t1 ++ ")"
       Apply t1 t2 -> "(" ++ show t1 ++ " " ++ show t2 ++ ")"
	

uniqueVar :: [String] -> String -> Int -> String
uniqueVar lst x n | elem (x ++ (show n)) lst = uniqueVar lst x (n+1)
                       | otherwise = x ++ (show n)

varSub :: [String] -> Term -> Term
varSub lst (Lambda x term) | elem x lst = Lambda x (varSub (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (varSub lst term)
varSub lst (Apply t1 t2) = Apply (varSub lst t1) (varSub lst t2)
varSub lst (Var x) | elem x lst = Var (uniqueVar lst x 0)
                                     | otherwise = Var x

eval1 :: Term -> Term
eval1 (Apply (Var x) t2) = (Apply (Var x) (eval1 t2))
eval1 (Apply (Lambda x t3) t2) = swapping [] t3 x t2
eval1 (Apply (Apply t3 t4) t2) = Apply (eval1 (Apply t3 t4)) t2
eval1 t = t

swapping :: [String] -> Term -> String -> Term -> Term 
swapping arr (Var y) x v = if x == y then (varSub arr v) else (Var y) 
swapping arr (Lambda y e) x v = if x == y then (Lambda y e) else (Lambda y (swapping (y:arr) e x v)) 
swapping arr (Apply e1 e2) x v = Apply (swapping arr e1 x v) (swapping arr e2 x v)


eval :: Term -> Term
eval (Apply (Var x) t2) = (Apply (Var x) (eval t2))
eval (Apply (Lambda x t3) t2) = eval $ swapping [] t3 x t2
eval (Apply (Apply t3 t4) t2) = eval $ Apply (eval (Apply t3 t4)) t2
eval t = t
-- Examples/Tests

example1 = Apply (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Var "z")) (Apply (Lambda "x" (Apply (Var "x") (Var "x"))) (Lambda "x" (Apply (Var "x") (Var "x"))))
example2 = Apply (Lambda "x" (Var "x")) (Lambda "z" (Var "z"))
example3 = Apply (Lambda "x" (Lambda "y" (Var "x"))) ((Lambda "z" (Var "z")))
example4 = Apply (Lambda "x" (Lambda "y" (Lambda "x" (Var "x")))) (Lambda "z" (Lambda "y" (Var "y")))
example5 = Apply (Apply (Lambda "x" (Var "x")) (Lambda "y" (Var "y"))) ((Lambda "z" (Var "z")))
example6 = Apply (Lambda "x" (Lambda "x" (Var "x"))) ((Lambda "z" (Var "z")))
example7 = Apply (Lambda "x" $ Lambda "y" $ Var "x") (Lambda "x" $ Var "y")
example10 = Apply (Apply (Apply (Lambda "n" $ Lambda "s" $ Lambda "z" $ Apply (Var "s") (Apply (Apply (Var "n") (Var "s")) (Var "z"))) (Lambda "s" $ Lambda "z" $ Apply (Var "s") (Var "z"))) (Var "S")) (Var "Z")
example9 = Apply (Apply (Apply (Apply (Lambda "m" $ Lambda "n" $ Lambda "s" $ Lambda "z" $ Apply (Apply (Var "m") (Apply (Var "n") (Var "s"))) (Var "z")) (Lambda "s" $ Lambda "z" $ Apply (Var "s") $ Apply (Var "s") $ Var "z")) (Lambda "s" $ Lambda "z" $ Apply (Var "s") $ Apply (Var "s") $ Var "z")) (Var "S")) (Var "Z")